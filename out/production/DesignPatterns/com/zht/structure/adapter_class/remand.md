## 类的适配器模式
有一个Source类，拥有一个方法，待适配，目标接口是Target，通过Adapter类，将Source的功能扩展到Target里。

### 步骤：
1. 创建source类，创建方法
2. 创建target接口
3. 创建adapter类 继承source类 实现target接口