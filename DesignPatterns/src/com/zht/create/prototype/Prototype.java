package com.zht.create.prototype;

/**
 * 原型模式
 * @author : zht
 * @date : 2022/7/29 14:39
 */
public class Prototype implements Cloneable{

    @Override
    public Object clone() throws CloneNotSupportedException{
        Prototype prototype = (Prototype) super.clone();
        return prototype;
    }
}
