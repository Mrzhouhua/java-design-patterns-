package com.zht.create.prototype;

import java.io.*;

/**
 * 深拷贝 浅拷贝
 * @author : zht
 * @date : 2022/7/29 14:41
 */
public class Copy implements Cloneable, Serializable {
    private static final long serialVersionUID = 1L;
    private String str;

    private SerializableObject obj;

    /** 浅拷贝 */
    @Override
    public Object clone() throws CloneNotSupportedException{
        Copy proto = (Copy) super.clone();
        return proto;
    }

    /** 深拷贝 */
    public Object deepClone() throws IOException, ClassNotFoundException{
        // 写入当前对象的二进制流
        ByteArrayOutputStream aos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(aos);
        oos.writeObject(this);

        // 读出二进制流产生的新对象
        ByteArrayInputStream ais = new ByteArrayInputStream(aos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(ais);
        return ois.readObject();
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public SerializableObject getObj() {
        return obj;
    }

    public void setObj(SerializableObject obj) {
        this.obj = obj;
    }
}

class SerializableObject implements Serializable{
    private static final long serialVersionUID = 1L;
}
