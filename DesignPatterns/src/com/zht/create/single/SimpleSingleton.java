package com.zht.create.single;

/**
 * 简单单例模式
 * 非线程安全
 * @author : zht
 * @date : 2022/7/28 15:34
 */
public class SimpleSingleton {

    /** 私有静态化实例 防止被引用。赋值null是为了实现懒加载 */
    private static SimpleSingleton instance = null;

    /** 私有构造化方法 防止被实例化 */
    private SimpleSingleton(){
    }

    /** 静态工厂方法，创建实例 */
    public static SimpleSingleton getInstance(){
        if(instance == null) {
            instance = new SimpleSingleton();
        }
        return instance;
    }

    /** 如果对象被序列化 保证对象在序列化前后一致 */
    public Object readResolve(){
        return instance;
    }

}
