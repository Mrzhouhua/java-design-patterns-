package com.zht.create.single;

/**
 * 线程安全的单例
 * @author : zht
 * @date : 2022/7/28 15:45
 */
public class SafeSingleton {

    private SafeSingleton(){}
    /** 性能差的实现方式
     *  给整个方法加锁 每次调用getInstance()都需要给对象加锁 */

    //private static SafeSingleton instance = null;

    //public static synchronized SafeSingleton getInstance(){
    //    if (instance == null){
    //        instance = new SafeSingleton();
    //    }
    //    return instance;
    //}

    /** 给对象加锁 只有在对象为空时需要加锁
     *  可能会出现对象未初始化问题
     *  Java指令中创建对象和赋值操作是分开进行的
     *  当A、B两线程同时进入了第一个if判断 A先进入synchronized块
     *  此时JVM内部先划出了一些分配给SafeSingleton实例的空白内存 并赋值给instance（但是此时还没有开始初始化）
     *  然后A离开了synchronized块 B线程此时打算使用instance 却发现未初始化 所以错误发生了 */
    //public static SafeSingleton getInstance(){
    //    if (instance == null){
    //        synchronized (instance){
    //            if (instance == null) {
    //                instance = new SafeSingleton();
    //            }
    //        }
    //    }
    //    return instance;
    //}

    /** 优化
     *  此处使用一个内部类来维护单例
     *  JVM内部的机制能够保证当一个类被加载的时候 这个类的加载过程是线程互斥的
     *  这样就不存在线程安全问题 当第一次调用getInstance()的时候能够保证instance只被创建一次并内存初始化 */

    private static class SafeSingletonFactory {
        private static SafeSingleton instance = new SafeSingleton();
    }

    public static SafeSingleton getInstance(){
        return SafeSingletonFactory.instance;
    }

    public Object readResolve(){
        return getInstance();
    }
}
