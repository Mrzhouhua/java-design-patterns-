package com.zht.create.simple_factory;

/**
 * 测试
 * @author : zht
 * @date : 2022/7/27 15:02
 */
public class FactoryTest {
    public static void main(String[] args) {
        // 普通工厂模式
        Factory factory = new Factory();
        Action bird = factory.produce("Bird");
        bird.fly();

        // 多个工厂方法模式
        factory.produceBird().fly();
        factory.produceButterfly().fly();

        // 静态工厂方法模式
        Factory.produceBirdStatic().fly();
        Factory.produceButterflyStatic().fly();
    }
}
