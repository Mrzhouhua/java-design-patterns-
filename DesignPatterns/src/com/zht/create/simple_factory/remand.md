##工厂方法模式
建立一个工厂类，对实现共同接口的类进行实例的创建

###步骤：
1. 创建共同接口，并定义方法
2. 创建类并实现接口
3. 创建工厂类，通过工程类创建类的实例化

###类型：
- 普通工厂模式
- 多个工厂方法模式
- 静态工厂方法模式

普通工厂方法模式需要传入参数，根据参数生成对应的实例；
多个工厂方法模式不需要传入参数，但需要实例化工厂类；
静态工厂方法模式不需要实例化工厂类就能直接对类进行实例的创建。