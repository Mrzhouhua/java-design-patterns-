package com.zht.create.simple_factory;

/**
 * 蝴蝶
 * @author : zht
 * @date : 2022/7/27 14:46
 */
public class Butterfly implements Action{
    @Override
    public void fly() {
        System.out.println("蝴蝶在飞......");
    }
}
