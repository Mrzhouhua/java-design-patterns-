package com.zht.create.simple_factory;

/**
 * 共同接口
 * @author : zht
 * @date : 2022/7/27 14:41
 */
public interface Action {
    /**
     * 飞
     */
    public void fly();
}
