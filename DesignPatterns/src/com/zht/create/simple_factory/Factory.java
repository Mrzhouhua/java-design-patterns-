package com.zht.create.simple_factory;

/**
 * 工厂类
 *
 * @author : zht
 * @date : 2022/7/27 14:48
 */
public class Factory {
    // 普通工厂模式
    public Action produce(String type) {
        if ("Bird".equals(type)) {
            return new Bird();
        } else if ("Butterfly".equals(type)) {
           return new Butterfly();
        }else {
            System.out.println("please input right animals name...");
            return null;
        }
    }

    // 多个工厂方法模式
    public Action produceBird(){
        return new Bird();
    }

    public Action produceButterfly(){
        return new Butterfly();
    }

    // 静态工厂方法模式

    public static Action produceBirdStatic(){
        return new Bird();
    }

    public static Action produceButterflyStatic(){
        return new Butterfly();
    }
}
