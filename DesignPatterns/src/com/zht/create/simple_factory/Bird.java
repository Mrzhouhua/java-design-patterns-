package com.zht.create.simple_factory;
/**
 * 鸟
 * @author : zht
 * @date : 2022/7/27 14:43
 */
public class Bird implements Action{
    @Override
    public void fly() {
        System.out.println("鸟在飞......");
    }
}
