package com.zht.create.abstract_factory;

/**
 * 测试
 * @author : zht
 * @date : 2022/7/27 18:07
 */
public class FactoryTest {
    public static void main(String[] args) {
        Provider provider = new BirdFactory();
        provider.produce().fly();
    }
}
