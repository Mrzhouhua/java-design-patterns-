package com.zht.create.abstract_factory;

/**
 * 生产
 * @author : zht
 * @date : 2022/7/27 18:04
 */
public interface Provider {
    /**
     * 生产
     * @return com.zht.factory.abstract_factory.Action
     */
    public Action produce();
}
