package com.zht.create.abstract_factory;

/**
 * 鸟
 * @author : zht
 * @date : 2022/7/27 18:01
 */
public class Bird implements Action{
    @Override
    public void fly() {
        System.out.println("鸟在飞......");
    }
}
