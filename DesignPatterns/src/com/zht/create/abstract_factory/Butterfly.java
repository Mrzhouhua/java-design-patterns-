package com.zht.create.abstract_factory;

/**
 * 蝴蝶
 * @author : zht
 * @date : 2022/7/27 18:02
 */
public class Butterfly implements Action{
    @Override
    public void fly() {
        System.out.println("蝴蝶在飞......");
    }
}
