package com.zht.create.abstract_factory;

/**
 * 鸟工厂
 * @author : zht
 * @date : 2022/7/27 18:03
 */
public class BirdFactory implements Provider{
    @Override
    public Action produce() {
        return new Bird();
    }
}
