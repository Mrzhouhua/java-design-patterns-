package com.zht.create.abstract_factory;

/**
 * 蝴蝶工厂
 * @author : zht
 * @date : 2022/7/27 18:06
 */
public class ButterflyFactory implements Provider{
    @Override
    public Action produce() {
        return new Butterfly();
    }
}
