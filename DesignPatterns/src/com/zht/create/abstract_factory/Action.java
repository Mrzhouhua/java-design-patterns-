package com.zht.create.abstract_factory;

/**
 * 共同接口
 * @author : zht
 * @date : 2022/7/27 18:01
 */
public interface Action {
    /**
     * 飞
     */
    public void fly();
}
