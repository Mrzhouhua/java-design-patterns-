package com.zht.create.build;

import java.util.ArrayList;
import java.util.List;

/**
 * 建造者模式
 * @author : zht
 * @date : 2022/7/28 17:43
 */
public class Builder {

    private List<Action> list = new ArrayList<>();

    public void produceBird(int num){
        for (int i = 0; i < num; i++) {
            list.add(new Bird());
        }
    }

    public void produceButterfly(int num){
        for (int i = 0; i < num; i++) {
            list.add(new Butterfly());
        }
    }
}
