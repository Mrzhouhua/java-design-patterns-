package com.zht.create.build;

/**
 * 蝴蝶
 * @author : zht
 * @date : 2022/7/28 17:40
 */
public class Butterfly implements Action {
    @Override
    public void fly() {
        System.out.println("蝴蝶在飞......");
    }
}
