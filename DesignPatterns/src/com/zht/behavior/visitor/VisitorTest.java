package com.zht.behavior.visitor;

/**
 * @author : zht
 * @date : 2022/8/3 10:37
 */
public class VisitorTest {
    public static void main(String[] args) {
        Subject subject = new MySubject();
        Visitor visitor = new MyVisitor();
        subject.accept(visitor);
    }
}
