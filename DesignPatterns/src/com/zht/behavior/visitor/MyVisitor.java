package com.zht.behavior.visitor;

/**
 * @author : zht
 * @date : 2022/8/3 10:30
 */
public class MyVisitor implements Visitor{
    @Override
    public void visit(Subject subject) {
        System.out.println("visit the subject: " + subject.getSubject());
    }
}
