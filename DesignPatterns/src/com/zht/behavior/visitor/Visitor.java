package com.zht.behavior.visitor;

/**
 * @author : zht
 * @date : 2022/8/3 10:29
 */
public interface Visitor {
    public void visit(Subject subject);
}
