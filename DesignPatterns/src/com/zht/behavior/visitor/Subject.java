package com.zht.behavior.visitor;

/**
 * @author : zht
 * @date : 2022/8/3 10:30
 */
public interface Subject {
    public void accept(Visitor visitor);
    public String getSubject();
}
