package com.zht.behavior.visitor;

/**
 * @author : zht
 * @date : 2022/8/3 10:33
 */
public class MySubject implements Subject{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getSubject() {
        return "accept";
    }
}
