package com.zht.behavior.mediator;

/**
 * @author : zht
 * @date : 2022/8/4 17:58
 */
public interface Mediator {

    public void createMeditor();

    public void workAll();
}
