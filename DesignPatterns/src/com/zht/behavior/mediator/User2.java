package com.zht.behavior.mediator;

/**
 * @author : zht
 * @date : 2022/8/4 17:59
 */
public class User2 extends User{

    public User2(Mediator mediator){
        super(mediator);
    }

    @Override
    public void work() {
        System.out.println("user2 exe");
    }
}
