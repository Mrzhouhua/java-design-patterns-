package com.zht.behavior.mediator;

/**
 * @author : zht
 * @date : 2022/8/4 17:58
 */
public abstract class User {
    private Mediator mediator;

    public User(Mediator mediator){
        this.mediator = mediator;
    }

    public abstract void work();
}
