package com.zht.behavior.mediator;

/**
 * @author : zht
 * @date : 2022/8/4 18:04
 */
public class MediatorTest {
    public static void main(String[] args) {
        Mediator mediator = new MyMediator();
        mediator.createMeditor();
        mediator.workAll();
    }
}
