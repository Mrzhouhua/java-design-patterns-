package com.zht.behavior.observer;

/**
 * @author : zht
 * @date : 2022/8/2 10:52
 */
public class MySubject extends AbstractSubject{
    @Override
    public void operation() {
        System.out.println("update self");
        notifyObserver();
    }
}
