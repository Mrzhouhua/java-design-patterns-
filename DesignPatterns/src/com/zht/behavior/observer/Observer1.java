package com.zht.behavior.observer;

/**
 * @author : zht
 * @date : 2022/8/2 10:44
 */
public class Observer1 implements Observer{
    @Override
    public void update() {
        System.out.println("Observer1 has received！");
    }
}
