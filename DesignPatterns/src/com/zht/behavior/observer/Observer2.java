package com.zht.behavior.observer;

/**
 * @author : zht
 * @date : 2022/8/2 10:45
 */
public class Observer2 implements Observer{
    @Override
    public void update() {
        System.out.println("Observer2 has received！");
    }
}
