package com.zht.behavior.observer;

/**
 * @author : zht
 * @date : 2022/8/2 10:53
 */
public class ObserverTest {
    public static void main(String[] args) {
        Subject subject = new MySubject();
        subject.add(new Observer1());
        subject.add(new Observer2());

        subject.operation();
    }
}
