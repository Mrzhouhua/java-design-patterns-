package com.zht.behavior.observer;

/**
 * @author : zht
 * @date : 2022/8/2 10:44
 */
public interface Observer {
    public void update();
}
