package com.zht.behavior.memento;

/**
 * @author : zht
 * @date : 2022/8/3 9:22
 */
public class MementoTest {
    public static void main(String[] args) {
        // 创建原始类
        Original original = new Original("ZHOU");

        // 创建存储备忘录类
        Storage storage = new Storage(original.createMemento());

        // 修改原始类的状态
        System.out.println("初始值为：" + original.getValue());
        original.setValue("ONG");
        System.out.println("修改后值为：" + original.getValue());

        // 恢复原始状态值
        original.restoreMemento(storage.getMemento());
        System.out.println("恢复后值为：" + original.getValue());
    }
}
