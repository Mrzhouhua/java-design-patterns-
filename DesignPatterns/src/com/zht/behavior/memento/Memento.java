package com.zht.behavior.memento;

/**
 * 备忘录类
 * @author : zht
 * @date : 2022/8/3 9:15
 */
public class Memento {
    private String value;

    public Memento(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
