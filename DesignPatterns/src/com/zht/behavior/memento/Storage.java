package com.zht.behavior.memento;

/**
 * 存储备忘录类
 * @author : zht
 * @date : 2022/8/3 9:17
 */
public class Storage {
    private Memento memento;

    public Storage(Memento memento) {
        this.memento = memento;
    }

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
