package com.zht.behavior.responsibility;

/**
 * @author : zht
 * @date : 2022/8/2 17:49
 */
public interface Handle {
    public void operation();
}
