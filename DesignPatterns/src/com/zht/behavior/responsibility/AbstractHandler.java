package com.zht.behavior.responsibility;

/**
 * @author : zht
 * @date : 2022/8/2 17:50
 */
public class AbstractHandler {
    private Handle handle;

    public Handle getHandle() {
        return handle;
    }

    public void setHandle(Handle handle) {
        this.handle = handle;
    }
}
