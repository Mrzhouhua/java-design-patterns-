package com.zht.behavior.responsibility;

/**
 * @author : zht
 * @date : 2022/8/2 17:51
 */
public class MyHander extends AbstractHandler implements Handle {

    private String name;

    public MyHander(String name) {
        this.name = name;
    }

    @Override
    public void operation() {
        System.out.println(name + "deal!");
        if (getHandle() != null) {
            getHandle().operation();
        }
    }
}
