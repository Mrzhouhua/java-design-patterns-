package com.zht.behavior.responsibility;

/**
 * @author : zht
 * @date : 2022/8/2 17:55
 * @description:
 */
public class ChainTest {
    public static void main(String[] args) {
        MyHander h1 = new MyHander("h1");
        MyHander h2 = new MyHander("h2");
        MyHander h3 = new MyHander("h3");
        h1.setHandle(h2);
        h2.setHandle(h3);
        h1.operation();
    }
}
