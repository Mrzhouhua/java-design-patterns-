package com.zht.behavior.strategy;

/**
 * @author : zht
 * @date : 2022/8/2 9:42
 */
public abstract class AbstractCalculator {
    public int[] split(String exp, String opt) {
        String arr[] = exp.split(opt);
        int arrayInt[] = new int[2];
        arrayInt[0] = Integer.parseInt(arr[0]);
        arrayInt[1] = Integer.parseInt(arr[1]);
        return arrayInt;
    }
}
