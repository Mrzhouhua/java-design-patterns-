package com.zht.behavior.strategy;

/**
 * @author : zht
 * @date : 2022/8/2 9:40
 */
public interface ICalculator {
    public int calculate(String exp);
}
