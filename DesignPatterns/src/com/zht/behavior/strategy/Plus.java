package com.zht.behavior.strategy;

/**
 * @author : zht
 * @date : 2022/8/2 9:47
 */
public class Plus extends AbstractCalculator implements ICalculator{
    @Override
    public int calculate(String exp) {
        int[] arrayInt = split(exp, "\\+");
        return arrayInt[0] + arrayInt[1];
    }
}
