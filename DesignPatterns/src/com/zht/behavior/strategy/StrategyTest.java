package com.zht.behavior.strategy;

/**
 * @author : zht
 * @date : 2022/8/2 9:52
 */
public class StrategyTest {
    public static void main(String[] args) {
        String exp = "6+2";
        ICalculator calculator = new Plus();
        int result = calculator.calculate(exp);
        System.out.println(result);
    }
}
