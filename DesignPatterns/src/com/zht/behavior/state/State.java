package com.zht.behavior.state;

/**
 * 状态类
 * @author : zht
 * @date : 2022/8/3 9:37
 */
public class State {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void method1(){
        System.out.println("this is opt1");
    }

    public void method2(){
        System.out.println("this is opt2");
    }
}
