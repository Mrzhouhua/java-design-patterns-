package com.zht.behavior.interpreter;

/**
 * @author : zht
 * @date : 2022/8/5 8:55
 */
public class InterpreterTest {
    public static void main(String[] args) {
        int result = new Minus().interpret(new Context(new Plus().interpret(new Context(2, 3)), 2));
        System.out.println(result);
    }
}
