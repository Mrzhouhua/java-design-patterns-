package com.zht.behavior.interpreter;

/**
 * @author : zht
 * @date : 2022/8/5 8:49
 */
public interface Expression {
    public int interpret(Context context);
}
