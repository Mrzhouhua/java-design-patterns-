package com.zht.behavior.interpreter;

/**
 * @author : zht
 * @date : 2022/8/5 8:51
 */
public class Plus implements Expression{

    @Override
    public int interpret(Context context) {
        return context.getNum1() + context.getNum2();
    }
}
