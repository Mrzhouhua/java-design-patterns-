package com.zht.behavior.command;

/**
 * @author : zht
 * @date : 2022/8/2 18:12
 */
public class Invoker {
    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void action(){
        command.exe();
    }
}
