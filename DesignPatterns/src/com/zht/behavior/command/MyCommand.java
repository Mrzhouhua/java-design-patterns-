package com.zht.behavior.command;

/**
 * @author : zht
 * @date : 2022/8/2 18:11
 * @description:
 */
public class MyCommand implements Command{
    private Receiver receiver;

    public MyCommand(Receiver receiver){
        this.receiver = receiver;
    }
    @Override
    public void exe() {
        receiver.action();
    }
}
