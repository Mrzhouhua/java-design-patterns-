package com.zht.behavior.command;

/**
 * @author : zht
 * @date : 2022/8/2 18:10
 */
public interface Command {
    public void exe();
}
