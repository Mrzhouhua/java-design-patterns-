package com.zht.behavior.command;

/**
 * @author : zht
 * @date : 2022/8/2 18:13
 */
public class CommandTest {
    public static void main(String[] args) {
        Receiver receiver = new Receiver();
        MyCommand myCommand = new MyCommand(receiver);
        Invoker invoker = new Invoker(myCommand);
        invoker.action();
    }
}
