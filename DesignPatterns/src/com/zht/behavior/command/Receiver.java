package com.zht.behavior.command;

/**
 * @author : zht
 * @date : 2022/8/2 18:10
 */
public class Receiver {
    public void action(){
        System.out.println("command received!");
    }
}
