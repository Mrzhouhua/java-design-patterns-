package com.zht.behavior.Iterator;

/**
 * @author : zht
 * @date : 2022/8/2 11:19
 */
public interface Iterator {

    /** 前移 */
    public Object previous();

    /** 后移 */
    public Object next();

    /** 判断是否还有 */
    public boolean hasNext();

    /** 取得第一个元素 */
    public Object first();
}
