package com.zht.behavior.Iterator;


/**
 * @author : zht
 * @date : 2022/8/2 11:17
 */
public interface Collection {

    public Iterator iterator();

    /** 获取集合元素 */
    public Object get(int i);

    /** 获取集合大小 */
    public int size();
}
