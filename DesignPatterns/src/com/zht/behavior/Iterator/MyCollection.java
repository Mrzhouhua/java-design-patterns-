package com.zht.behavior.Iterator;


/**
 * @author : zht
 * @date : 2022/8/2 11:36
 */
public class MyCollection implements Collection{

    private String[] strings = {"A","B","C","D","E","F"};

    @Override
    public Iterator iterator() {
        return new MyIterator(this);
    }

    @Override
    public Object get(int i) {
        return strings[i];
    }

    @Override
    public int size() {
        return strings.length;
    }
}
