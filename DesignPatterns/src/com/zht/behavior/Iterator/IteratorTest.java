package com.zht.behavior.Iterator;

/**
 * @author : zht
 * @date : 2022/8/2 11:39
 */
public class IteratorTest {
    public static void main(String[] args) {
        Collection collection = new MyCollection();
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
