package com.zht.behavior.template;

/**
 * @author : zht
 * @date : 2022/8/2 10:14
 */
public class Plus extends AbstractCalculator{
    @Override
    public int calculate(int num1, int num2) {
        return num1 + num2;
    }
}
