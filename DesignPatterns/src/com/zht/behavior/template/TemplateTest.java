package com.zht.behavior.template;

/**
 * @author : zht
 * @date : 2022/8/2 10:15
 */
public class TemplateTest {
    public static void main(String[] args) {
        String exp = "2+6";
        AbstractCalculator calculator = new Plus();
        int result = calculator.calculate(exp, "\\+");
        System.out.println(result);

        String exp1 = "6-3";
        AbstractCalculator calculator1 = new Minus();
        int result1 = calculator1.calculate(exp1, "-");
        System.out.println(result1);
    }
}
