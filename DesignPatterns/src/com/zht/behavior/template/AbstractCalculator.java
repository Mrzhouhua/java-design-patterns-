package com.zht.behavior.template;

/**
 * @author : zht
 * @date : 2022/8/2 10:10
 */
public abstract class AbstractCalculator {

    public final int calculate(String exp, String opt) {
        int[] arrayInt = split(exp, opt);
        return calculate(arrayInt[0], arrayInt[1]);
    }

    public abstract int calculate(int num1, int num2);

    public int[] split(String exp, String opt) {
        String[] arr = exp.split(opt);
        int[] arrayInt = new int[2];
        arrayInt[0] = Integer.parseInt(arr[0]);
        arrayInt[1] = Integer.parseInt(arr[1]);
        return arrayInt;
    }
}
