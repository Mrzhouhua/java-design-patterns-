package com.zht.structure.proxy;

/**
 * @author : zht
 * @date : 2022/7/29 16:45
 */
public class ProxyTest {
    public static void main(String[] args) {
        Sourceable sourceable = new Proxy();
        sourceable.method();
    }
}
