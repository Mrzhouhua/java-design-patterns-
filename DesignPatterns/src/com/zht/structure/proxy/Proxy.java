package com.zht.structure.proxy;

/**
 * @author : zht
 * @date : 2022/7/29 16:43
 */
public class Proxy implements Sourceable{
    private Source source;

    public Proxy(){
        super();
        this.source = new Source();
    }

    @Override
    public void method() {
        before();
        source.method();
        after();
    }

    private void before(){
        System.out.println("source method run before");
    }

    private void after(){
        System.out.println("source method run after");
    }
}
