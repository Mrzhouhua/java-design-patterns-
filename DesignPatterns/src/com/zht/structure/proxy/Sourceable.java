package com.zht.structure.proxy;

/**
 * @author : zht
 * @date : 2022/7/29 16:39
 */
public interface Sourceable {
    public void method();
}
