package com.zht.structure.proxy;

/**
 * @author : zht
 * @date : 2022/7/29 16:42
 */
public class Source implements Sourceable{

    @Override
    public void method() {
        System.out.println("this is source method......");
    }
}
