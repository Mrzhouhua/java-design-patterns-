package com.zht.structure.adapter_obj;

/**
 * @author : zht
 * @date : 2022/7/29 15:38
 * @description:
 */
public interface Target {

    public void method1();
    public void method2();
}
