package com.zht.structure.adapter_obj;

/**
 * @author : zht
 * @date : 2022/7/29 15:38
 */
public class Wrapper implements Target {
    private Source source;

    public Wrapper(Source source){
        super();
        this.source = source;
    }

    @Override
    public void method1() {
        source.method1();
    }

    @Override
    public void method2() {
        System.out.println("this is target method......");
    }
}
