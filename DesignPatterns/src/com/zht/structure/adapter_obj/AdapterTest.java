package com.zht.structure.adapter_obj;

/**
 * 对象的适配器模式
 * @author : zht
 * @date : 2022/7/29 15:40
 */
public class AdapterTest {
    public static void main(String[] args) {
        Source source = new Source();
        Target target = new Wrapper(source);
        target.method1();
        target.method2();

    }
}
