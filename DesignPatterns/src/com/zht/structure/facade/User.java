package com.zht.structure.facade;

/**
 * @author : zht
 * @date : 2022/8/1 14:49
 */
public class User {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.startup();
        computer.shutdown();
    }
}
