package com.zht.structure.facade;

/**
 * @author : zht
 * @date : 2022/8/1 14:45
 */
public class Disk {
    public void startup() {
        System.out.println("disk startup......");
    }

    public void shutdown() {
        System.out.println("disk shutdown......");
    }
}
