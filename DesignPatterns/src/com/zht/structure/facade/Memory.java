package com.zht.structure.facade;

/**
 * @author : zht
 * @date : 2022/8/1 14:45
 */
public class Memory {
    public void startup() {
        System.out.println("memory startup......");
    }

    public void shutdown() {
        System.out.println("memory shutdown......");
    }
}
