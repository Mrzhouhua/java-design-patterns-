package com.zht.structure.facade;

/**
 * @author : zht
 * @date : 2022/8/1 14:43
 */
public class CPU {
    public void startup() {
        System.out.println("cpu startup......");
    }

    public void shutdown() {
        System.out.println("cpu shutdown......");
    }
}
