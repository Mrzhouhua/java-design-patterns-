package com.zht.structure.bridge;

/**
 * @author : zht
 * @date : 2022/8/1 16:28
 */
public class MyBridge extends Bridge{
    @Override
    public void method(){
        getSourceable().method();
    }
}
