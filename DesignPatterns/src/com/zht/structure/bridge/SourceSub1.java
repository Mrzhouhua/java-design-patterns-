package com.zht.structure.bridge;

/**
 * @author : zht
 * @date : 2022/8/1 16:25
 */
public class SourceSub1 implements Sourceable{
    @Override
    public void method() {
        System.out.println("this is the SourceSub1...");
    }
}
