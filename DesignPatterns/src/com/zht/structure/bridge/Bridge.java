package com.zht.structure.bridge;

/**
 * @author : zht
 * @date : 2022/8/1 16:27
 */
public class Bridge {
    private Sourceable sourceable;

    public void method(){
        sourceable.method();
    }

    public Sourceable getSourceable() {
        return sourceable;
    }

    public void setSourceable(Sourceable sourceable) {
        this.sourceable = sourceable;
    }
}
