package com.zht.structure.bridge;

/**
 * @author : zht
 * @date : 2022/8/1 16:22
 */
public interface Sourceable {
    public void method();
}
