package com.zht.structure.adapter_class;

/**
 * @author : zht
 * @date : 2022/7/29 15:30
 */
public class AdapterTest {
    public static void main(String[] args) {
        Target target = new Adapter();
        target.method1();
        target.method2();
    }
}
