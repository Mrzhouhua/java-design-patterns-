package com.zht.structure.adapter_class;

/**
 * @author : zht
 * @date : 2022/7/29 15:28
 */
public class Adapter extends Source implements Target{

    @Override
    public void method2() {
        System.out.println("this is target method......");
    }
}
