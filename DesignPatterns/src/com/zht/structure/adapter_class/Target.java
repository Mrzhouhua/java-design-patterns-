package com.zht.structure.adapter_class;

/**
 * @author : zht
 * @date : 2022/7/29 15:29
 */
public interface Target {

    public void method1();
    public void method2();
}
