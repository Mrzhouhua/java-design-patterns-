package com.zht.structure.composite;


/**
 * @author : zht
 * @date : 2022/8/1 16:54
 */
public class Tree {
    private TreeNode root = null;

    public Tree(String name){
        root = new TreeNode(name);
    }

    public static void main(String[] args) {
        Tree tree = new Tree("root");
        TreeNode first = new TreeNode("first");
        TreeNode second = new TreeNode("second");
        first.add(second);
        tree.root.add(first);
    }
}
