package com.zht.structure.adapter_interface;

/**
 * @author : zht
 * @date : 2022/7/29 15:52
 */
public class SourceSub1 extends Wrapper{
    @Override
    public void method1() {
        System.out.println("this is SourceSub1 method......");
    }
}
