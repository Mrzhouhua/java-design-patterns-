package com.zht.structure.adapter_interface;

/**
 * @author : zht
 * @date : 2022/7/29 15:53
 */
public class SourceSub2 extends Wrapper {
    @Override
    public void method2() {
        System.out.println("this is SourceSub2 method......");
    }
}
