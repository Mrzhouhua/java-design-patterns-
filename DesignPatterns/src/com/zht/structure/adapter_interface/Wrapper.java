package com.zht.structure.adapter_interface;

/**
 * @author : zht
 * @date : 2022/7/29 15:52
 */
public abstract class Wrapper implements Sourceable {
    @Override
    public void method1() {

    }

    @Override
    public void method2() {

    }
}
