package com.zht.structure.adapter_interface;

/**
 * @author : zht
 * @date : 2022/7/29 15:51
 */
public interface Sourceable {
    public void method1();
    public void method2();
}
