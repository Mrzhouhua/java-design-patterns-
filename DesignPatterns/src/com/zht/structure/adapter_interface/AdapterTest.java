package com.zht.structure.adapter_interface;

/**
 * @author : zht
 * @date : 2022/7/29 15:54
 * @description:
 */
public class AdapterTest {
    public static void main(String[] args) {
        Sourceable sub1 = new SourceSub1();
        Sourceable sub2 = new SourceSub2();

        sub1.method1();
        sub1.method2();
        sub2.method1();
        sub2.method2();
    }
}
