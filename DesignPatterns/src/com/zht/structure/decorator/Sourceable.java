package com.zht.structure.decorator;

/**
 * @author : zht
 * @date : 2022/7/29 16:06
 */
public interface Sourceable {
    public void method();
}
