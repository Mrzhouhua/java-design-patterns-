package com.zht.structure.decorator;

/**
 * @author : zht
 * @date : 2022/7/29 16:09
 */
public class DecoratorTest {
    public static void main(String[] args) {
        Source source = new Source();
        Decorator decorator = new Decorator(source);
        decorator.method();
    }
}
