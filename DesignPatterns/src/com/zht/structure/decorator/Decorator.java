package com.zht.structure.decorator;

/**
 * @author : zht
 * @date : 2022/7/29 16:07
 */
public class Decorator implements Sourceable{

    private Source source;

    public Decorator(Source source){
        super();
        this.source = source;
    }

    @Override
    public void method() {
        System.out.println("source method run before");
        source.method();
        System.out.println("source method run after");
    }
}
