package com.zht.structure.decorator;

/**
 * @author : zht
 * @date : 2022/7/29 16:06
 */
public class Source implements Sourceable{
    @Override
    public void method() {
        System.out.println("this is source method...");
    }
}
