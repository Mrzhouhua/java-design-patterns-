# Java设计模式

#### 顺序
单例模式（Singleton）
工厂方法模式（Factory Method）
抽象工厂模式
建造者模式（Builder）
原型模式（Prototype）

适配器模式（Adapter）
装饰模式
代理者模式（Proxy）
外观模式（Facade）
桥接模式
组合模式（Composite）
享元模式（Flyweight）

策略模式（strategy）
模板方法模式（Template Method）
观察者模式（Observer）
迭代子模式（Iterator）
责任链模式（Chain of Responsibility）
命令模式（Command）
备忘录模式（Memento）
状态模式（State）
访问者模式（Visitor）
中介者模式（Mediator）
解释器模式（Interpreter）

来自博客：
https://blog.csdn.net/sugar_no1/article/details/88317950
